//
//  FeedTableViewController.h
//  #music
//
//  Created by Ambas Chobsanti on 6/13/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface FeedTableViewController : BaseTableViewController

@property (assign, nonatomic) BOOL isSeeker;
@property BOOL isRated;

@end
