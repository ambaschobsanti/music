//
//  ShopFeedCell.h
//  #music
//
//  Created by Ambas Chobsanti on 6/14/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopFeedCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *shopImage;
@property (weak, nonatomic) IBOutlet UILabel *shopName;
@property (weak, nonatomic) IBOutlet UILabel *location;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UILabel *genre0;
@property (weak, nonatomic) IBOutlet UILabel *genre1;

@end
