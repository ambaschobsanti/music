//
//  FormTableViewController.m
//  #music
//
//  Created by Ambas Chobsanti on 6/13/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

#import "FormTableViewController.h"
#import "FeedTableViewController.h"
#import "UIViewController+Push.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface FormTableViewController () <UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) UITextField *currentActiveTextField;
@property (strong, nonatomic) UIPickerView *typePicker;
@property (strong, nonatomic) UIPickerView *seekerPicker;
@property (strong, nonatomic) UIPickerView *musicianPicker;

@property (strong, nonatomic) UIPickerView *genrePicker;
@property (strong, nonatomic) UIPickerView *rangPicker;

@end

@implementation FormTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.typePicker = [[UIPickerView alloc] init];
    self.seekerPicker = [[UIPickerView alloc] init];
    self.musicianPicker = [[UIPickerView alloc] init];
    self.genrePicker = [[UIPickerView alloc] init];
    self.rangPicker = [[UIPickerView alloc] init];
    self.typePicker.delegate = self;
    self.typePicker.dataSource = self;
    self.seekerPicker.delegate = self;
    self.seekerPicker.dataSource = self;
    self.musicianPicker.delegate = self;
    self.musicianPicker.dataSource = self;
    self.musicianType.inputView = self.typePicker;
    self.seekerType.inputView = self.typePicker;
    self.seekerPlace.inputView = self.seekerPicker;
        self.musicianPlace.inputView = self.musicianPicker;
    self.genrePicker.delegate = self;
    self.rangPicker.delegate = self;
    
    self.musicianGenrePicker.inputView = self.genrePicker;
    self.musicianPriceRangeTextField.inputView = self.rangPicker;
    
    self.shopGenreTextField.inputView = self.genrePicker;
    self.shopPriceRangeTextField.inputView = self.rangPicker;
    
   }

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSArray *musicianType = @[@"Individual",@"Band"];
    NSArray *seekerPlaceType = @[@"restaurant", @"pub",@"restaurant", @"pub",@"restaurant", @"pub"];
    NSArray *genre = @[ @"Acoustic",
                        @"Alternative",
                        @"Classic",
                        @"Country",
                        @"Dance",
                        @"Electronic",
                        @"Hiphop/Rap",
                        @"Jazz",
                        @"Pop",
                        @"R&B Soul",
                        @"Reggae Ska",
                        @"Rock"];
    NSArray *range = @[@"300 - 500",
                       @"500 - 1,000",
                       @"1,000 - 2,000",
                       @"2,000 - 5,000",
                       @"5,000 - 10,000",
                       @"10,000 - 20,000",
                       @"Don't Care"];
    NSString *textToSet = @"";
    if (pickerView == self.typePicker) {
        textToSet =  musicianType[row];
    } else if (pickerView == self.seekerPicker) {
        textToSet = musicianType[row];
    } else if (pickerView == self.musicianPicker) {
        textToSet =  seekerPlaceType[row];
    } else if (pickerView == self.genrePicker) {
        textToSet = genre[row];
    } else if (pickerView == self.rangPicker) {
        textToSet =range[row];
    }
    self.currentActiveTextField.text = textToSet;
}

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == self.typePicker) {
    return 2;
    } else if (pickerView == self.seekerPicker) {
        return 2;
    } else if (pickerView == self.musicianPicker) {
        return 5;
    } else if(pickerView == self.genrePicker) {
        return 12;
    } else if (pickerView == self.rangPicker) {
        return 7;
    }
    return  0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSArray *musicianType = @[@"Individual",@"Band"];
    NSArray *seekerPlaceType = @[@"restaurant", @"pub",@"restaurant", @"pub",@"restaurant", @"pub"];
    NSArray *genre = @[ @"Acoustic",
                        @"Alternative",
                        @"Classic",
                        @"Country",
                        @"Dance",
                        @"Electronic",
                        @"Hiphop/Rap",
                        @"Jazz",
                        @"Pop",
                        @"R&B Soul",
                        @"Reggae Ska",
                        @"Rock"];
    NSArray *range = @[@"300 - 500",
                       @"500 - 1,000",
                       @"1,000 - 2,000",
                       @"2,000 - 5,000",
                       @"5,000 - 10,000",
                       @"10,000 - 20,000",
                       @"Don't Care"];
    if (pickerView == self.typePicker) {
        return musicianType[row];
    } else if (pickerView == self.seekerPicker) {
        return musicianType[row];
    } else if (pickerView == self.musicianPicker) {
        return seekerPlaceType[row];
    } else if (pickerView == self.genrePicker) {
        return genre[row];
    } else if (pickerView == self.rangPicker) {
        return range[row];
    }
    return row == 0 ? @"Individual" : @"Band";
}

- (IBAction)doneForSeeker:(id)sender {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
    hud.detailsLabelText = @"Analyzing...";
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [hud hide:YES];
        [self pushToViewControllerWithIdentifier:@"feed" storyboardName:@"Feed" configurationHandler:^(id viewController) {
            FeedTableViewController * feedVC = viewController;
            feedVC.isSeeker = YES;
        } animation:nil completion:nil];
    });
}
- (IBAction)doneForMusician:(id)sender {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
    hud.detailsLabelText = @"Analyzing...";
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [hud hide:YES];
        [self pushToViewControllerWithIdentifier:@"feed" storyboardName:@"Feed" configurationHandler:^(id viewController) {
            FeedTableViewController * feedVC = viewController;
            feedVC.isSeeker = NO;
        } animation:nil completion:nil];
    });

}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.currentActiveTextField = textField;
}

@end
