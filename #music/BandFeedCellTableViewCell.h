//
//  BandFeedCellTableViewCell.h
//  #music
//
//  Created by Ambas Chobsanti on 6/14/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BandFeedCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bandImageView;
@property (weak, nonatomic) IBOutlet UILabel *bandName;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UILabel *rage;
@property (weak, nonatomic) IBOutlet UILabel *genre0;
@property (weak, nonatomic) IBOutlet UILabel *genre1;
@property (weak, nonatomic) IBOutlet UIImageView *star;
@property (weak, nonatomic) IBOutlet UIImageView *typeIcon;

@end
