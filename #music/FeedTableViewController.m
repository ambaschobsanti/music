//
//  FeedTableViewController.m
//  #music
//
//  Created by Ambas Chobsanti on 6/13/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

#import "FeedTableViewController.h"
#import "BandFeedCellTableViewCell.h"
#import "ShopFeedCell.h"
#import "UIViewController+Push.h"
#import "DetailTableViewController.h"

@interface FeedTableViewController ()

@property (weak, nonatomic) IBOutlet UILabel *subHeadLabel;
@property (weak, nonatomic) IBOutlet UILabel *subHeadLabel2;
@property (strong, nonatomic) NSArray *arr;

@end

@implementation FeedTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.subHeadLabel.hidden = !self.isSeeker;
    self.subHeadLabel2.hidden = self.isSeeker;
    NSString *path;
    if (self.isSeeker) {
        path = [[NSBundle mainBundle] pathForResource:@"brand_and_dj" ofType:@"json"];
    } else {
        path = [[NSBundle mainBundle] pathForResource:@"shop" ofType:@"json"];
    }
    
    NSData *jsonData = [NSData dataWithContentsOfFile:path];
    self.arr = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];
    }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.isSeeker ? 125 : 200;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *data = self.arr[indexPath.row];
    if (self.isSeeker) {
      NSString *cellId = @"feedCell";
        BandFeedCellTableViewCell *cell = (BandFeedCellTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
        cell.bandName.text = data[@"name"];
        NSString *photoName = [NSString stringWithFormat:@"%@.jpg",data[@"photo"]];
        cell.bandImageView.image = [UIImage imageNamed:photoName];
        cell.rage.text = data[@"price"];
        cell.genre0.text = data[@"genre"][0];
        cell.genre1.text = data[@"genre"][1];
        int rating = [data[@"rate"] integerValue];
        UIImage *starImage;
        if (rating == 3) {
            starImage = [UIImage imageNamed:@"star3"];
        } else if (rating == 4) {
            starImage = [UIImage imageNamed:@"star4"];
            
        } else if (rating ==5 ) {
            starImage = [UIImage imageNamed:@"star5"];
        }
        cell.star.image = starImage;
        NSString *type = data[@"type"];
        cell.type.text = type;
        if ([type isEqualToString:@"band"]) {
            cell.typeIcon.image = [UIImage imageNamed:@"musicianTypeIcon"];
        } else {
           cell.typeIcon.image = [UIImage imageNamed:@"musicianTypeIcon2"];
        }
        return cell;
    } else {
        NSString *cellId = @"shopCell";
        ShopFeedCell *cell = (ShopFeedCell *)[self.tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
        cell.shopName.text = data[@"name"];
        cell.location.text = data[@"location"];
        cell.type.text = data[@"type"];
        cell.genre0.text = data[@"genre"][0];
        cell.genre1.text = data[@"genre"][1];
        NSString *photoName = [NSString stringWithFormat:@"%@.jpg",data[@"photo"]];
        cell.shopImage.image = [UIImage imageNamed:photoName];
        return cell;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   [self pushToViewControllerWithIdentifier:@"detail" storyboardName:@"Feed" configurationHandler:^(id viewController) {
       DetailTableViewController *detailVC = viewController;
       detailVC.detail = self.arr[1];
   } animation:nil completion:nil];
}

@end
