//
//  UIViewController+Push.h
//  #music
//
//  Created by Ambas Chobsanti on 6/13/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Push)

- (void)pushToViewControllerWithIdentifier:(NSString *)identifier
                            storyboardName:(NSString *)storyboardName
                      configurationHandler:(void (^)(id viewController))configurationHandler
                                 animation:(void (^)(void))animation
                                completion:(void (^)(void))completion;

- (void)showError:(NSError *)error;

+ (id)tw_viewControllerFromStoryboardName:(NSString *)storyboardName;
+ (id)tw_viewControllerFromStoryboardName:(NSString *)storyboardName storyboardID:(NSString *)storyboardID;

@end
