//
//  UIViewController+Push.m
//  #music
//
//  Created by Ambas Chobsanti on 6/13/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

#import "UIViewController+Push.h"

@implementation UIViewController (Push)

- (void)pushToViewControllerWithIdentifier:(NSString *)identifier
                            storyboardName:(NSString *)storyboardName
                      configurationHandler:(void (^)(id viewController))configurationHandler
                                 animation:(void (^)(void))animation
                                completion:(void (^)(void))completion {
    //If nil is provided, we use current storyboard and cache the viewcontroller.
    UIStoryboard *targetStoryboard = (storyboardName) ? [UIStoryboard storyboardWithName:storyboardName bundle:nil] : self.storyboard;
    UIViewController *targetViewController = [targetStoryboard instantiateViewControllerWithIdentifier:identifier];
    
    if (targetViewController && configurationHandler) {
        configurationHandler(targetViewController);
    }
    [self.navigationController pushViewController:targetViewController animated:YES];
    
    if (completion) {
        completion();
    }
}

- (void)showError:(NSError *)error {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:error.localizedDescription message:error.localizedRecoverySuggestion preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:OKAction];
    
    [self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];
}

+ (id)tw_viewControllerFromStoryboardName:(NSString *)storyboardName {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    return [storyboard instantiateInitialViewController];
}

+ (id)tw_viewControllerFromStoryboardName:(NSString *)storyboardName storyboardID:(NSString *)storyboardID {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:storyboardID];
}


@end
