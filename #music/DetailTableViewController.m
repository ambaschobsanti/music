//
//  DetailTableViewController.m
//  #music
//
//  Created by Amornchai Kanokpullwad on 6/13/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

@import MessageUI;

#import "DetailTableViewController.h"

@interface DetailTableViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation DetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *photoName = [NSString stringWithFormat:@"%@.jpg",self.detail[@"photo"]];
    self.coverImage.image = [UIImage imageNamed:photoName];
    self.descriptionLabel.text = self.detail[@"description"];
    
    self.navigationItem.title = self.detail[@"name"];
    NSArray *genres = self.detail[@"genre"];
    NSString *genre = [NSString stringWithFormat:@"%@ , %@",genres[0], genres[1]];
    self.genreLabel.text = genre;
}

- (IBAction)requestDidTouch:(id)sender {
    [self.acceptButton setTitle:@"Requested!" forState:UIControlStateNormal];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Detail" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *call = [UIAlertAction actionWithTitle:@"Call" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSString *phoneNumber = [@"tel://" stringByAppendingString:self.detail[@"tel"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }];
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
       MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:@[self.detail[@"email"]]];
        [self presentViewController:controller animated:YES completion:nil];
    }];
    UIAlertAction *facebook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//        NSString *phoneNumber = [@"fb://" stringByAppendingString:self.detail[@"fb"]];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.detail[@"fb"]]];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:call];
    [alertController addAction:email];
    [alertController addAction:facebook];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
