//
//  DetailTableViewController.h
//  #music
//
//  Created by Amornchai Kanokpullwad on 6/13/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseTableViewController.h"

@interface DetailTableViewController : BaseTableViewController
@property (weak, nonatomic) IBOutlet UIImageView *coverImage;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (strong, nonatomic) NSDictionary *detail;

@end
