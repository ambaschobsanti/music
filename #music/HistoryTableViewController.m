//
//  HistoryTableViewController.m
//  #music
//
//  Created by Amornchai Kanokpullwad on 6/14/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

#import "HistoryTableViewController.h"
#import "FeedTableViewController.h"

@interface HistoryTableViewController ()

@property (weak, nonatomic) IBOutlet UILabel *rateLabel;

@end

@implementation HistoryTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (((FeedTableViewController *)((UINavigationController *) self.parentViewController.presentingViewController).topViewController).isRated) {
        self.rateLabel.text = @"rated";
    }
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 1;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSString *cellId = @"Cell1";
//    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
//    
//    return cell;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"showRate" sender:self];
}

@end
