//
//  FormTableViewController.h
//  #music
//
//  Created by Ambas Chobsanti on 6/13/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface FormTableViewController : BaseTableViewController
@property (weak, nonatomic) IBOutlet UITextField *musicianType;
@property (weak, nonatomic) IBOutlet UITextField *seekerType;

@property (weak, nonatomic) IBOutlet UITextField *musicianPlace;
@property (weak, nonatomic) IBOutlet UITextField *seekerPlace;

@property (weak, nonatomic) IBOutlet UITextField *shopGenreTextField;
@property (weak, nonatomic) IBOutlet UITextField *shopPriceRangeTextField;

@property (weak, nonatomic) IBOutlet UITextField *musicianGenrePicker;
@property (weak, nonatomic) IBOutlet UITextField *musicianPriceRangeTextField;


@end
