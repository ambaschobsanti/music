//
//  ReviewTableViewController.m
//  #music
//
//  Created by Amornchai Kanokpullwad on 6/14/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

#import "ReviewTableViewController.h"
#import "FeedTableViewController.h"

@interface ReviewTableViewController ()

@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView;
@property int count;

@end

@implementation ReviewTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.count = 5;
}

- (IBAction)plusButtonPressed:(id)sender {
    if (self.count < 5) {
        self.count++;
        self.starImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"star%d", self.count]];
        self.countLabel.text = [NSString stringWithFormat:@"%d", self.count];
    }
}

- (IBAction)minusButtonPressed:(id)sender {
    if (self.count > 1) {
        self.count--;
        self.starImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"star%d", self.count]];
        self.countLabel.text = [NSString stringWithFormat:@"%d", self.count];
    }
}

- (IBAction)submitButtonPressed:(id)sender {
    ((FeedTableViewController *)((UINavigationController *) self.parentViewController.presentingViewController).topViewController).isRated = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
