//
//  BaseTableViewController.m
//  #music
//
//  Created by Ambas Chobsanti on 6/13/2558 BE.
//  Copyright (c) 2558 AM. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor blackColor];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background2"]];
}


@end
